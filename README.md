# LackLuster Android Application #

# Purpose #

LackLuster is an android application which allows its users to create, share and synchronize lists. A list can contain multiple items, where an item is represented as a check box associated with a label. Users can share any list with any one else who has the application installed on his/her device. The shared lists will be automatically synchronized once the devices are in contact. Data will be shared through NFC (Near Field Communication), that is, actions such as list sharing and synchronizing between devices will be carried out through a NFC connection.

# Scope #

Any individual who knows how to operate an android device is adequate to use this application.

# Operating Environment #

1. Android OS enabled device (version 4.1+)
2. NFC hardware must be present in the device