package com.atish.lackluster.fragment;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.atish.lackluster.R;
import com.atish.lackluster.adapter.ListMetadataAdapter;
import com.atish.lackluster.dialog.CreateListDialog;
import com.atish.lackluster.manager.DbManager;
import com.atish.lackluster.manager.LogManager;
import com.atish.lackluster.manager.UtilityManager;
import com.atish.lackluster.model.ListModel;

import java.util.ArrayList;

public class HomeFragment extends Fragment implements View.OnClickListener{

    private static final String TAG="HomeFragment";

    private AppCompatActivity mActivity;
    private FloatingActionButton mAddList;
    private ArrayList<ListModel> mLists;
    private ListView mListView;
    private DbManager mDbManager;
    private ListMetadataAdapter mListMetadataAdapter;
    private CoordinatorLayout mPushUpLayout;

    public HomeFragment() {
        LogManager.log(TAG, "<<constructor>>");
    }

    public static HomeFragment getInstance(){
        LogManager.log(TAG, "getInstance");
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        LogManager.log(TAG, "onCreate");
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        LogManager.log(TAG, "onCreateView");
        setRetainInstance(true);
        View rootView=inflater.inflate(R.layout.fragment_home, container, false);
        mActivity=(AppCompatActivity)getActivity();
        View listFooter=mActivity.getLayoutInflater().inflate(R.layout.list_footer,null);
        View listHeader=mActivity.getLayoutInflater().inflate(R.layout.list_header,null);
        mDbManager=new DbManager(mActivity);
        mAddList=(FloatingActionButton)rootView.findViewById(R.id.add_list);
        mListView=(ListView)rootView.findViewById(R.id.list);
        mPushUpLayout=(CoordinatorLayout)rootView.findViewById(R.id.push_up_layout);
        mAddList.setOnClickListener(this);
        mListView.addFooterView(listFooter);
        mListView.addHeaderView(listHeader);
        return rootView;
    }

    @Override
    public void onResume() {
        LogManager.log(TAG, "onResume");
        super.onResume();
        mDbManager.open();
        mLists=mDbManager.getLists();
        ListModel.sort(mLists);
        mListMetadataAdapter=new ListMetadataAdapter(mActivity,mLists,mDbManager,this);
        mListView.setAdapter(mListMetadataAdapter);
    }

    @Override
    public void onPause() {
        LogManager.log(TAG, "onPause");
        super.onPause();
        mDbManager.close();
    }

    @Override
    public void onDestroy(){
        LogManager.log(TAG, "onDestroy");
        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        LogManager.log(TAG, "onSaveInstanceState");
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onClick(View view){
        LogManager.log(TAG, "onClick");
        switch(view.getId()){
            case R.id.add_list:
                addList();
        }
    }

    public void updateLists(){
        LogManager.log(TAG, "updateLists");
        int prevSize=mLists.size();
        mLists.clear();
        ArrayList<ListModel> lists=mDbManager.getLists();
        for(ListModel l:lists){
            mLists.add(l);
        }
        ListModel.sort(mLists);
        mListMetadataAdapter.notifyDataSetChanged();
        if(mLists.size()>prevSize){
            UtilityManager.snackbar(mPushUpLayout,R.string.new_list_created);
        }else if(prevSize>mLists.size()){
            UtilityManager.snackbar(mPushUpLayout,R.string.list_deleted);
        }
    }

    private void addList(){
        LogManager.log(TAG, "addList");
        CreateListDialog createListDialog=new CreateListDialog(mActivity,mDbManager,this);
        createListDialog.show();
    }

}
