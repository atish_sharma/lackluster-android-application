package com.atish.lackluster.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.atish.lackluster.R;
import com.atish.lackluster.manager.LogManager;
import com.atish.lackluster.manager.UtilityManager;

/**
 * A simple {@link Fragment} subclass.
 */
public class AboutFragment extends Fragment {

    private static final String TAG="AboutFragment";

    public AboutFragment() {
        LogManager.log(TAG, "<<constructor>>");
    }

    public static AboutFragment getInstance(){
        LogManager.log(TAG,"getInstance");
        AboutFragment fragment = new AboutFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        LogManager.log(TAG,"onCreateView");
        setRetainInstance(true);
        View rootView=inflater.inflate(R.layout.fragment_about, container, false);
        return rootView;
    }

}
