package com.atish.lackluster.fragment;


import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.atish.lackluster.R;
import com.atish.lackluster.activity.EditListActivity;
import com.atish.lackluster.dialog.CreateItemDialog;
import com.atish.lackluster.manager.DbManager;
import com.atish.lackluster.manager.LogManager;
import com.atish.lackluster.model.ItemModel;
import com.atish.lackluster.model.ListModel;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditListFragment extends Fragment implements View.OnClickListener{


    private static final String TAG="EditListFragment";

    private AppCompatActivity mActivity;
    private FloatingActionButton mAddList;
    private ArrayList<String> mItems;
    private ArrayList<ItemModel> mItemModels;
    private ListView mListView;
    private DbManager mDbManager;
    private ArrayAdapter<String> mAdapter;
    private CoordinatorLayout mPushUpLayout;
    private ListModel mListModel;

    public EditListFragment() {
        LogManager.log(TAG, "<<constructor>>");
    }

    public static EditListFragment getInstance(){
        LogManager.log(TAG, "getInstance");
        EditListFragment fragment = new EditListFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        LogManager.log(TAG, "onCreate");
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        LogManager.log(TAG, "onCreateView");
        setRetainInstance(true);
        View rootView=inflater.inflate(R.layout.fragment_edit_list, container, false);
        mActivity=(AppCompatActivity)getActivity();
        mItems=new ArrayList<String>();
        View listFooter=mActivity.getLayoutInflater().inflate(R.layout.list_footer,null);
        View listHeader=mActivity.getLayoutInflater().inflate(R.layout.list_header,null);
        mDbManager=new DbManager(mActivity);
        mAddList=(FloatingActionButton)rootView.findViewById(R.id.add_item);
        mListView=(ListView)rootView.findViewById(R.id.list);
        mPushUpLayout=(CoordinatorLayout)rootView.findViewById(R.id.push_up_layout);
        mAddList.setOnClickListener(this);
        mListView.addFooterView(listFooter);
        mListView.addHeaderView(listHeader);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view,int position, long id) {
                position--;
                if(position>=0 && position<mItemModels.size()){
                    mItemModels.get(position).setChecked(!mItemModels.get(position).isChecked());
                    mItemModels.get(position).setUpdateTimestamp(System.currentTimeMillis());
                    mListModel.setUpdateTimestamp(System.currentTimeMillis());
                    updateItems();
                }
            }
        });
        return rootView;
    }

    @Override
    public void onResume() {
        LogManager.log(TAG, "onResume");
        super.onResume();
        mDbManager.open();
        mListModel=mDbManager.getList(EditListActivity.LIST_UUID);
        mItemModels=mListModel.getItems();
        setItems();
        mAdapter=new ArrayAdapter(mActivity, android.R.layout.simple_list_item_multiple_choice, mItems);
        mListView.setAdapter(mAdapter);
        setChecks();
    }

    @Override
    public void onPause() {
        LogManager.log(TAG, "onPause");
        super.onPause();
        mDbManager.close();
    }

    @Override
    public void onDestroy(){
        LogManager.log(TAG, "onDestroy");
        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        LogManager.log(TAG, "onSaveInstanceState");
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onClick(View view){
        LogManager.log(TAG, "onClick");
        switch(view.getId()){
            case R.id.add_item:
                addItem();
        }
    }

    private void addItem(){
        LogManager.log(TAG,"addItem");
        CreateItemDialog createItemDialog=new CreateItemDialog(mActivity,mListModel,this,mPushUpLayout);
        createItemDialog.show();
    }

    private void setItems(){
        LogManager.log(TAG, "setItems");
        mItems.clear();
        for(ItemModel item:mItemModels){
            mItems.add(item.getLabel());
        }
    }

    private void setChecks(){
        LogManager.log(TAG, "setChecks");
        for(int i=0;i<mItemModels.size();i++){
            mListView.setItemChecked(i+1,mItemModels.get(i).isChecked());
        }
    }

    public void updateItems(){
        LogManager.log(TAG, "updateItems");
        mDbManager.updateList(mListModel);
        setItems();
        mAdapter.notifyDataSetChanged();
        setChecks();
    }

}
