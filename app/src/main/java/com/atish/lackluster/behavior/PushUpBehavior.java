package com.atish.lackluster.behavior;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.util.AttributeSet;
import android.view.View;

import com.atish.lackluster.manager.LogManager;

/**
 * Created by atish on 27/4/16.
 */
public class PushUpBehavior extends CoordinatorLayout.Behavior<View>{

    private static final String TAG="PushUpBehavior";

    public PushUpBehavior(){
        super();
        LogManager.log(TAG, "<<constructor>>");
    }

    public PushUpBehavior(Context context, AttributeSet attrs) {
        super(context,attrs);
        LogManager.log(TAG,"<<constructor>>");
    }

    @Override
    public boolean layoutDependsOn(CoordinatorLayout parent, View child, View dependency) {
        LogManager.log(TAG,"layoutDependsOn");
        return dependency instanceof Snackbar.SnackbarLayout;
    }

    @Override
    public boolean onDependentViewChanged(CoordinatorLayout parent, View child, View dependency) {
        LogManager.log(TAG,"onDependentViewChanged");
        float translationY = Math.min(0, dependency.getTranslationY() - dependency.getHeight());
        child.setTranslationY(translationY);
        return true;
    }

}
