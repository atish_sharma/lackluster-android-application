package com.atish.lackluster.manager;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.atish.lackluster.model.ListModel;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by atish on 27/4/16.
 */
public class DbManager {

    private static final String TAG="DbManager";
    private static final String DB_NAME="lackluster";
    private static final String TABLE_LISTS="lists";

    private SQLiteDatabase mDatabase;
    private Context mContext;
    private Gson mGson;

    public DbManager(Context context){
        LogManager.log(TAG,"<<constructor>>");
        mContext=context;
        mGson=new Gson();
    }

    public void open(){
        LogManager.log(TAG, "open");
        try{
            mDatabase=mContext.openOrCreateDatabase(DB_NAME, mContext.MODE_PRIVATE, null);
            String query=String.format("CREATE TABLE IF NOT EXISTS %s (uuid VARCHAR PRIMARY KEY,list VARCHAR NOT NULL);",TABLE_LISTS);
            mDatabase.execSQL(query);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void close(){
        LogManager.log(TAG, "close");
        try{
            mDatabase.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public ListModel getList(String uuid){
        LogManager.log(TAG,"getList");
        try{
            String query=String.format("SELECT list FROM %s WHERE uuid = '%s' ;", TABLE_LISTS, uuid);
            Cursor cursor=mDatabase.rawQuery(query,null);
            if(cursor.getCount()>0){
                cursor.moveToFirst();
                String list=cursor.getString(0);
                ListModel listModel=(ListModel)UtilityManager.jsonToObject(new JSONObject(list),ListModel.class);
                return listModel;
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public ArrayList<ListModel> getLists(){
        LogManager.log(TAG,"getLists");
        try{
            String query=String.format("SELECT list FROM %s ;",TABLE_LISTS);
            Cursor cursor=mDatabase.rawQuery(query,null);
            cursor.moveToFirst();
            ArrayList<ListModel> lists=new ArrayList<ListModel>();
            for(int i=0;i<cursor.getCount();i++,cursor.moveToNext()){
                String list=cursor.getString(0);
                ListModel listModel=(ListModel)UtilityManager.jsonToObject(new JSONObject(list),ListModel.class);
                lists.add(listModel);
            }
            return lists;
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public boolean listExists(ListModel listModel){
        LogManager.log(TAG,"listExists");
        try{
            ListModel lm=getList(listModel.getUuid());
            return lm==null?false:true;
        }catch(Exception e){
            e.printStackTrace();
        }
        return false;
    }

    public void updateList(ListModel list){
        LogManager.log(TAG,"updateList");
        try{
            String listJson=mGson.toJson(list);
            String query=String.format("UPDATE %s SET list = '%s' WHERE uuid = '%s';",TABLE_LISTS,listJson,list.getUuid());
            mDatabase.execSQL(query);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void deleteList(String uuid){
        LogManager.log(TAG,"deleteList");
        try{
            mDatabase.delete(TABLE_LISTS,String.format("uuid = '%s'",uuid),null);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void addList(ListModel list){
        LogManager.log(TAG,"addList");
        try{
            String listJson=mGson.toJson(list);
            String query=String.format("INSERT INTO %s VALUES('%s', '%s');",TABLE_LISTS,list.getUuid(),listJson);
            mDatabase.execSQL(query);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

}
