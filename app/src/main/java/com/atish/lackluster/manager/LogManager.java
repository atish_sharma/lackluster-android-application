package com.atish.lackluster.manager;

import android.util.Log;

/**
 * Created by atish on 26/4/16.
 */
public class LogManager {

    public static void log(final String tag,final String msg){
        if(ConfigManager.LOG){
            Log.d(tag,msg);
        }
    }

}
