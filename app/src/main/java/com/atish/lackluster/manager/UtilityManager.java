package com.atish.lackluster.manager;

import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import org.json.JSONObject;

import java.util.UUID;

/**
 * Created by atish on 27/4/16.
 */
public class UtilityManager {

    private static final String TAG="UtilityManager";

    public static void toast(AppCompatActivity activity, String message) {
        LogManager.log(TAG, "toast");
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
    }

    public static void snackbar(View view, int stringResourceId) {
        LogManager.log(TAG, "snackbar");
        Snackbar.make(view, stringResourceId, Snackbar.LENGTH_SHORT).show();
    }

    public static Object jsonToObject(JSONObject json, Class className) {
        LogManager.log(TAG, "jsonToObject");
        Gson gson = new Gson();
        String jsonStr = json.toString();
        JsonElement jsonElement = new JsonParser().parse(jsonStr);
        return gson.fromJson(jsonElement, className);
    }

    public static String generateUuid(){
        LogManager.log(TAG, "generateUuid");
        UUID uuid = UUID.fromString("38400000-8cf0-11bd-b23e-10b96e4ef00d");
        return uuid.randomUUID().toString();
    }

    public static void setScreen(AppCompatActivity activity, int frameId, Fragment fragment, String tag) {
        LogManager.log(TAG, "setScreen");
        try {
            FragmentTransaction fragmentTransaction = activity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(frameId, fragment, tag);
            fragmentTransaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            LogManager.log(TAG, e.getMessage());
        }
    }

}
