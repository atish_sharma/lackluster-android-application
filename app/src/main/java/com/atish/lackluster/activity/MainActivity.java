package com.atish.lackluster.activity;

import android.content.Intent;
import android.nfc.NfcAdapter;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.atish.lackluster.R;
import com.atish.lackluster.adapter.HomePagerAdapter;
import com.atish.lackluster.manager.LogManager;
import com.atish.lackluster.manager.UtilityManager;
import com.atish.lackluster.module.NfcModule;

public class MainActivity extends AppCompatActivity {

    private static final String TAG="MainActivity";

    private AppCompatActivity mActivity;
    private ViewPager mViewPager;
    private HomePagerAdapter mHomePagerAdapter;
    private boolean mBackPressed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        LogManager.log(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mActivity=this;
        mViewPager=(ViewPager)findViewById(R.id.pager);
        mHomePagerAdapter = new HomePagerAdapter(mActivity.getSupportFragmentManager());
        mViewPager.setAdapter(mHomePagerAdapter);
        mViewPager.setOffscreenPageLimit(mHomePagerAdapter.getCount());
        setTab(0);
    }

    @Override
    protected void onResume(){
        LogManager.log(TAG, "onResume");
        super.onResume();
        Intent intent = getIntent();
        if(NfcAdapter.ACTION_NDEF_DISCOVERED.equals(intent.getAction())){
            UtilityManager.toast(mActivity, NfcModule.getNdefMessageFromIntent(intent));
        }
    }

    @Override
    protected void onPause(){
        LogManager.log(TAG, "onPause");
        super.onPause();
    }

    @Override
    public void onDestroy(){
        LogManager.log(TAG, "onDestroy");
        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        LogManager.log(TAG, "onSaveInstanceState");
        super.onSaveInstanceState(outState);
    }

    private void setTab(int position){
        LogManager.log(TAG, "setTab");
        mViewPager.setCurrentItem(position);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        LogManager.log(TAG, "onCreateOptionsMenu");
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        LogManager.log(TAG, "onOptionsItemSelected");
        switch(item.getItemId()){
            case R.id.action_about:
                startActivity(new Intent(MainActivity.this,AboutActivity.class));
                return true;
            case R.id.action_exit:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed(){
        LogManager.log(TAG,"onBackPressed");
        if (mBackPressed){
            super.onBackPressed();
            return;
        }
        mBackPressed= true;
        UtilityManager.toast(this, getResources().getString(R.string.text_back_pressed));
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mBackPressed = false;
            }
        }, getResources().getInteger(R.integer.back_press_time));
    }

}
