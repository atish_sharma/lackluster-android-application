package com.atish.lackluster.activity;

import android.content.Intent;
import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;
import android.nfc.NfcEvent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.nfc.NfcAdapter.CreateNdefMessageCallback;
import android.nfc.NfcAdapter.OnNdefPushCompleteCallback;
import android.view.MenuItem;

import com.atish.lackluster.R;
import com.atish.lackluster.adapter.EditListPagerAdapter;
import com.atish.lackluster.manager.ConfigManager;
import com.atish.lackluster.manager.DbManager;
import com.atish.lackluster.manager.LogManager;
import com.atish.lackluster.manager.UtilityManager;
import com.atish.lackluster.model.ListModel;
import com.atish.lackluster.module.NfcModule;
import com.google.gson.Gson;

import org.json.JSONObject;

public class EditListActivity extends AppCompatActivity implements CreateNdefMessageCallback,OnNdefPushCompleteCallback {

    private static final String TAG="EditListActivity";

    public static String LIST_UUID="";

    private NfcAdapter mNfcAdapter;
    private AppCompatActivity mActivity;
    private ViewPager mViewPager;
    private EditListPagerAdapter mEditListPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        LogManager.log(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mActivity=this;
        mNfcAdapter=NfcAdapter.getDefaultAdapter(mActivity);
        if (mNfcAdapter==null) {
            UtilityManager.toast(mActivity, getResources().getString(R.string.nfc_adapter_unavailable));
            finish();
            return;
        }
        if(!mNfcAdapter.isEnabled()){
            UtilityManager.toast(mActivity,getResources().getString(R.string.nfc_adapter_not_enabled));
            finish();
            return;
        }
        mNfcAdapter.setNdefPushMessageCallback(this, this);
        mNfcAdapter.setOnNdefPushCompleteCallback(this, this);
        mViewPager=(ViewPager)findViewById(R.id.pager);
        mEditListPagerAdapter = new EditListPagerAdapter(mActivity.getSupportFragmentManager());
        mViewPager.setAdapter(mEditListPagerAdapter);
        mViewPager.setOffscreenPageLimit(mEditListPagerAdapter.getCount());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        LogManager.log(TAG,"onOptionsItemSelected");
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return true;
    }

    @Override
    protected void onResume(){
        LogManager.log(TAG, "onResume");
        super.onResume();
        Intent intent = getIntent();
        if(NfcAdapter.ACTION_NDEF_DISCOVERED.equals(intent.getAction())){
            try {
                ListModel listModel = (ListModel) UtilityManager.jsonToObject(new JSONObject(NfcModule.getNdefMessageFromIntent(intent)), ListModel.class);
                LIST_UUID=listModel.getUuid();
                DbManager db=new DbManager(mActivity);
                db.open();
                if(db.listExists(listModel)){
                    db.updateList(listModel);
                }else{
                    db.addList(listModel);
                }
                db.close();
                getSupportActionBar().setTitle(listModel.getName());
            }catch(Exception e){
                e.printStackTrace();
                finish();
                UtilityManager.toast(mActivity,getResources().getString(R.string.nfc_parse_error));
            }
        }else{
            LIST_UUID=intent.getStringExtra(ConfigManager.INTENT_EXTRA_UUID);
            DbManager db=new DbManager(mActivity);
            db.open();
            ListModel listModel = db.getList(LIST_UUID);
            db.close();
            getSupportActionBar().setTitle(listModel.getName());
        }
        setTab(0);
    }

    @Override
    protected void onPause(){
        LogManager.log(TAG, "onPause");
        super.onPause();
    }

    @Override
    public void onDestroy(){
        LogManager.log(TAG, "onDestroy");
        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        LogManager.log(TAG, "onSaveInstanceState");
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onNewIntent(Intent intent) {
        LogManager.log(TAG, "onNewIntent");
        setIntent(intent);
    }

    private void setTab(int position){
        LogManager.log(TAG, "setTab");
        mViewPager.setCurrentItem(position);
    }

    @Override
    public void onNdefPushComplete(NfcEvent event) {
        LogManager.log(TAG, "onNdefPushComplete");
    }

    @Override
    public NdefMessage createNdefMessage(NfcEvent event) {
        LogManager.log(TAG, "createNdefMessage");
        DbManager db=new DbManager(mActivity);
        Gson gson=new Gson();
        db.open();
        ListModel listModel=db.getList(LIST_UUID);
        db.close();
        String listJson=gson.toJson(listModel);
        return NfcModule.createNdefMessage(this,listJson);
    }
}
