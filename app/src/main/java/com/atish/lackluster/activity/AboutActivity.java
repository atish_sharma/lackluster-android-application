package com.atish.lackluster.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import com.atish.lackluster.R;
import com.atish.lackluster.fragment.AboutFragment;
import com.atish.lackluster.manager.LogManager;
import com.atish.lackluster.manager.UtilityManager;

public class AboutActivity extends AppCompatActivity{

    private static final String TAG="AboutActivity";
    private static final String ABOUT_FRAGMENT_TAG="AboutActivityAboutFragment";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        LogManager.log(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setAboutScreen();
    }

    private void setAboutScreen(){
        LogManager.log(TAG, "setAboutScreen");
        AboutFragment aboutFragment=(AboutFragment)getSupportFragmentManager().findFragmentByTag(ABOUT_FRAGMENT_TAG);
        if(aboutFragment==null){
            UtilityManager.setScreen(this, R.id.about_frame, AboutFragment.getInstance(), ABOUT_FRAGMENT_TAG);
        }else{
            UtilityManager.setScreen(this, R.id.about_frame,aboutFragment,ABOUT_FRAGMENT_TAG);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        LogManager.log(TAG, "onOptionsItemSelected");
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return true;
    }

}
