package com.atish.lackluster.view;

/**
 * Created by atish on 27/4/16.
 */

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import com.atish.lackluster.behavior.PushUpBehavior;
import com.atish.lackluster.manager.LogManager;

@CoordinatorLayout.DefaultBehavior(PushUpBehavior.class)
public class PushUpRelativeLayout extends RelativeLayout {

    private static final String TAG="PushUpRelativeLayout";

    public PushUpRelativeLayout(Context context) {
        super(context);
        LogManager.log(TAG, "<<constructor>>");
    }

    public PushUpRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        LogManager.log(TAG, "<<constructor>>");
    }

    public PushUpRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        LogManager.log(TAG, "<<constructor>>");
    }

}

