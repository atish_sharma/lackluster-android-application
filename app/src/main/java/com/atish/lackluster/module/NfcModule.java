package com.atish.lackluster.module;

import android.content.Context;
import android.content.Intent;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.os.Parcelable;

import com.atish.lackluster.R;
import com.atish.lackluster.manager.LogManager;

/**
 * Created by atish on 26/4/16.
 */
public class NfcModule {

    private static final String TAG="NfcModule";

    public static NdefMessage createNdefMessage(final Context context,final String msg){
        LogManager.log(TAG,"createNdefMessage");
        NdefRecord ndefRecord = new NdefRecord(NdefRecord.TNF_MIME_MEDIA, "text/plain".getBytes(), new byte[] {}, msg.getBytes());
        NdefMessage ndefMessage=new NdefMessage(ndefRecord);
        return ndefMessage;
    }

    public static String getNdefMessageFromIntent(final Intent intent){
        LogManager.log(TAG,"getNdefMessageFromIntent");
        Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
        NdefMessage msg = (NdefMessage) rawMsgs[0];
        return new String(msg.getRecords()[0].getPayload());
    }

}
