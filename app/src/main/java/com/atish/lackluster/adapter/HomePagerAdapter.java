package com.atish.lackluster.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.atish.lackluster.fragment.HomeFragment;
import com.atish.lackluster.manager.LogManager;

/**
 * Created by atish on 27/4/16.
 */
public class HomePagerAdapter extends FragmentStatePagerAdapter {

    private static final String TAG="HomePagerAdapter";
    private static final int TAB_COUNT=1;

    public HomePagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
        LogManager.log(TAG,"<<constrcutor>>");
    }

    @Override
    public Fragment getItem(int index) {
        LogManager.log(TAG, "getItem");
        switch (index) {
            case 0:
                return HomeFragment.getInstance();
        }
        return null;
    }

    @Override
    public int getCount() {
        LogManager.log(TAG,"getCount");
        return TAB_COUNT;
    }

}
