package com.atish.lackluster.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.atish.lackluster.R;
import com.atish.lackluster.activity.EditListActivity;
import com.atish.lackluster.fragment.HomeFragment;
import com.atish.lackluster.manager.ConfigManager;
import com.atish.lackluster.manager.DbManager;
import com.atish.lackluster.manager.LogManager;
import com.atish.lackluster.model.ListModel;

import java.util.ArrayList;

/**
 * Created by atish on 27/4/16.
 */
public class ListMetadataAdapter extends ArrayAdapter<ListModel> {

    private static final String TAG="ListMetadataAdapter";
    private static final int LAYOUT = R.layout.card_list_metadata;

    private AppCompatActivity mActivity;
    private ArrayList<ListModel> mList;
    private DbManager mDbManager;
    private HomeFragment mHomeFragment;
    private TextView mTitle;
    private TextView mDescription;
    private TextView mUpdate;
    private ImageView mEdit;
    private ImageView mDelete;

    public ListMetadataAdapter(AppCompatActivity activity, ArrayList<ListModel> list,DbManager dbManager,HomeFragment homeFragment) {
        super(activity, LAYOUT, list);
        LogManager.log(TAG, "<<constructor>>");
        mActivity=activity;
        mList=list;
        mDbManager=dbManager;
        mHomeFragment=homeFragment;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rootView = inflater.inflate(LAYOUT, parent, false);
        final ListModel card=mList.get(position);
        mTitle=(TextView)rootView.findViewById(R.id.title);
        mDescription=(TextView)rootView.findViewById(R.id.description);
        mUpdate=(TextView)rootView.findViewById(R.id.update);
        mEdit=(ImageView)rootView.findViewById(R.id.edit);
        mDelete=(ImageView)rootView.findViewById(R.id.delete);
        mEdit.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                LogManager.log(TAG, card.getUuid());
                Intent intent=new Intent(mActivity, EditListActivity.class);
                intent.putExtra(ConfigManager.INTENT_EXTRA_UUID,card.getUuid());
                mActivity.startActivity(intent);
            }

        });
        mDelete.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                LogManager.log(TAG, card.getUuid());
                AlertDialog alertDialog=new AlertDialog.Builder(mActivity).create();
                alertDialog.setTitle(R.string.title_dialog_delete_list);
                alertDialog.setMessage(mActivity.getResources().getString(R.string.message_dialog_delete_list));
                alertDialog.setCancelable(true);
                alertDialog.setCanceledOnTouchOutside(true);
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, mActivity.getResources().getString(R.string.dialog_delete_list_button_positive), new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        mDbManager.deleteList(card.getUuid());
                        mHomeFragment.updateLists();
                    }

                });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, mActivity.getResources().getString(R.string.dialog_delete_list_button_negative), new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                    }

                });
                alertDialog.show();
            }

        });
        mTitle.setText(card.getName());
        mDescription.setText(card.getDescription());
        mUpdate.setText(card.getUpdateTime());
        return rootView;
    }

}
