package com.atish.lackluster.model;

/**
 * Created by atish on 27/4/16.
 */
public class ItemModel {

    private static final String TAG="ItemModel";

    private boolean checked;
    private String label;
    private String uuid;
    private long updateTimestamp;

    public long getUpdateTimestamp() {
        return updateTimestamp;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public void setUpdateTimestamp(long updateTimestamp) {
        this.updateTimestamp = updateTimestamp;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}
