package com.atish.lackluster.model;

import android.content.ClipData;

import com.atish.lackluster.adapter.ListMetadataAdapter;
import com.atish.lackluster.manager.LogManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Locale;

/**
 * Created by atish on 27/4/16.
 */
public class ListModel {

    private static final String TAG="ItemModel";
    public static final long NOT_UPDATED=0;

    private String uuid;
    private String name;
    private String description;
    private long updateTimestamp;
    private ArrayList<ItemModel> items;

    public ListModel(String uuid,String name,String description,long updateTimestamp){
        this.uuid=uuid;
        this.name=name;
        this.description=description;
        this.updateTimestamp=updateTimestamp;
        items=new ArrayList<ItemModel>();
    }

    public ListModel(){
        this(null,null,null,NOT_UPDATED);
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<ItemModel> getItems() {
        return items;
    }

    public void setItems(ArrayList<ItemModel> items) {
        this.items = items;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getUpdateTimestamp() {
        return updateTimestamp;
    }

    public void setUpdateTimestamp(long updateTimestamp) {
        this.updateTimestamp = updateTimestamp;
    }

    public String getUpdateTime(){
        LogManager.log(TAG,"getUpdateTimestamp");
        if(updateTimestamp==NOT_UPDATED){
            return "No update till now";
        }
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.UK);
        Calendar calendar=Calendar.getInstance();
        calendar.setTimeInMillis(updateTimestamp);
        String time=simpleDateFormat.format(calendar.getTime());
        return String.format("Last updated on %s",time);
    }

    public static void sort(ArrayList<ListModel> lists){
        LogManager.log(TAG,"sort");
        for(int i=0;i<lists.size();i++){
            for(int j=0;j<lists.size()-1;j++){
                if(lists.get(j).getUpdateTimestamp()<lists.get(j+1).getUpdateTimestamp()){
                    ListModel lm1=lists.get(j);
                    ListModel lm2=lists.get(j+1);
                    lists.set(j,lm2);
                    lists.set(j+1,lm1);
                }
            }
        }
    }

}
