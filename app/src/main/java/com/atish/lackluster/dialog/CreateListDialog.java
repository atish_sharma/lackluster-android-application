package com.atish.lackluster.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.atish.lackluster.R;
import com.atish.lackluster.adapter.ListMetadataAdapter;
import com.atish.lackluster.fragment.HomeFragment;
import com.atish.lackluster.manager.DbManager;
import com.atish.lackluster.manager.LogManager;
import com.atish.lackluster.manager.UtilityManager;
import com.atish.lackluster.model.ListModel;

import org.w3c.dom.Text;

/**
 * Created by atish on 27/4/16.
 */
public class CreateListDialog extends AlertDialog implements View.OnClickListener {

    private static final String TAG="CreateListDialog";

    private AppCompatActivity mActivity;
    private DbManager mDbManager;
    private HomeFragment mHomeFragment;
    private EditText mName;
    private EditText mDescription;
    private TextInputLayout mNameTextInputLayout;
    private TextInputLayout mDescriptionTextInputLayout;
    private Button mCreate;
    private Button mCancel;

    public CreateListDialog(AppCompatActivity activity,DbManager dbManager,HomeFragment homeFragment){
        super(activity);
        LogManager.log(TAG, "<<constructor>>");
        mActivity=activity;
        mDbManager=dbManager;
        mHomeFragment=homeFragment;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        LogManager.log(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_create_list);
        mName=(EditText)findViewById(R.id.list_title);
        mDescription=(EditText)findViewById(R.id.list_description);
        mNameTextInputLayout=(TextInputLayout)findViewById(R.id.list_title_textinputlayout);
        mDescriptionTextInputLayout=(TextInputLayout)findViewById(R.id.list_description_textinputlayout);
        mCreate=(Button)findViewById(R.id.create);
        mCancel=(Button)findViewById(R.id.cancel);
        mCreate.setOnClickListener(this);
        mCancel.setOnClickListener(this);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
    }

    @Override
    public void onClick(View view) {
        LogManager.log(TAG, "onClick");
        switch (view.getId()){
            case R.id.cancel:
                clickCancel();
                break;
            case R.id.create:
                clickCreate();
        }
    }

    private void clickCancel(){
        LogManager.log(TAG, "clickCancel");
        dismiss();
    }

    private void clickCreate(){
        LogManager.log(TAG, "clickCreate");
        String name=mName.getText().toString().trim();
        String description=mDescription.getText().toString().trim();
        mNameTextInputLayout.setErrorEnabled(false);
        mDescriptionTextInputLayout.setErrorEnabled(false);
        boolean error=false;
        if(name.equals("")){
            error=true;
            mNameTextInputLayout.setErrorEnabled(true);
            mNameTextInputLayout.setError(mActivity.getResources().getString(R.string.error_no_list_name));
        }
        if(description.equals("")){
            error=true;
            mDescriptionTextInputLayout.setErrorEnabled(true);
            mDescriptionTextInputLayout.setError(mActivity.getResources().getString(R.string.error_no_list_description));
        }
        if(!error){
            ListModel listModel=new ListModel(UtilityManager.generateUuid(),name,description,System.currentTimeMillis());
            mDbManager.addList(listModel);
            mHomeFragment.updateLists();
            dismiss();
        }
        mName.setText(mName.getText().toString());
        mDescription.setText(mDescription.getText().toString());
    }

}
