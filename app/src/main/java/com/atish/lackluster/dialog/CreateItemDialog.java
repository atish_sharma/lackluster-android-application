package com.atish.lackluster.dialog;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.atish.lackluster.R;
import com.atish.lackluster.fragment.EditListFragment;
import com.atish.lackluster.manager.LogManager;
import com.atish.lackluster.manager.UtilityManager;
import com.atish.lackluster.model.ItemModel;
import com.atish.lackluster.model.ListModel;
import com.atish.lackluster.view.PushUpRelativeLayout;

/**
 * Created by atish on 27/4/16.
 */
public class CreateItemDialog extends AlertDialog implements View.OnClickListener{

    private static final String TAG="CreateItemDialog";

    private AppCompatActivity mActivity;
    private ListModel mList;
    private EditListFragment mEditListFragment;
    private EditText mName;
    private TextInputLayout mNameTextInputLayout;
    private Button mCreate;
    private Button mCancel;
    private CoordinatorLayout mPushUpRelativeLayout;

    public CreateItemDialog(AppCompatActivity activity,ListModel list,EditListFragment editListFragment,CoordinatorLayout pushUpRelativeLayout){
        super(activity);
        LogManager.log(TAG, "<<constructor>>");
        mActivity=activity;
        mList=list;
        mEditListFragment=editListFragment;
        mPushUpRelativeLayout=pushUpRelativeLayout;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        LogManager.log(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_edit_list);
        mName=(EditText)findViewById(R.id.item_title);
        mNameTextInputLayout=(TextInputLayout)findViewById(R.id.item_title_textinputlayout);
        mCreate=(Button)findViewById(R.id.create);
        mCancel=(Button)findViewById(R.id.cancel);
        mCreate.setOnClickListener(this);
        mCancel.setOnClickListener(this);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
    }

    @Override
    public void onClick(View view) {
        LogManager.log(TAG, "onClick");
        switch (view.getId()){
            case R.id.cancel:
                clickCancel();
                break;
            case R.id.create:
                clickCreate();
        }
    }

    private void clickCancel(){
        LogManager.log(TAG, "clickCancel");
        dismiss();
    }

    private void clickCreate(){
        LogManager.log(TAG, "clickCreate");
        String name=mName.getText().toString().trim();
        mNameTextInputLayout.setErrorEnabled(false);
        boolean error=false;
        if(name.equals("")){
            error=true;
            mNameTextInputLayout.setErrorEnabled(true);
            mNameTextInputLayout.setError(mActivity.getResources().getString(R.string.error_no_item_name));
        }
        if(!error){
            ItemModel item=new ItemModel();
            item.setLabel(name);
            item.setUpdateTimestamp(System.currentTimeMillis());
            item.setChecked(false);
            item.setUuid(UtilityManager.generateUuid());
            mList.getItems().add(item);
            mEditListFragment.updateItems();
            UtilityManager.snackbar(mPushUpRelativeLayout,R.string.new_item_created);
            dismiss();
        }
        mName.setText(mName.getText().toString());
    }


}
